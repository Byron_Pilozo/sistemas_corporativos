
/*==============================================================*/
/* Table: DIM_CLIENTE                                           */
/*==============================================================*/
create table DIM_CLIENTE
(
   ID_CLIENTE           int not null,
   Nombre               varchar(45) not null,
   Num_ced_cliente              varchar(11) not null,
   primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Table: DIM_CONTRATO                                          */
/*==============================================================*/
create table DIM_CONTRATO
(
   ID_contrato          int not null,
   Tipo_de_plan         varchar(45) not null,
   Costo_plan              float,
   Descripcion          varchar(50) not null,
   Fecha                date,
   primary key (ID_contrato)
);

/*==============================================================*/
/* Table: HC_CARACTERISTICA                                     */
/*==============================================================*/
create table HC_CARACTERISTICA
(
   ID_contrato           int not null,
   ID_Cliente            int not null,
   Direccion            varchar(45) not null,
   primary key (ID_contrato, ID_Cliente, Direccion)
);
